from appJar import gui
import random
import os
import csv


# Establish the path to the script
path = os.path.dirname(__file__)

# Name the app, set icon, font, shape of app window
app = gui("Moneyball Simulator")

app.setResizable(True)
app.setResizable(canResize=True)
app.setSize(800, 500)
app.setLocation(x = 300, y = 200)
<<<<<<< HEAD
#comment out fira sans for windows install
app.setFont(20)#, font="Fira Sans")
=======
app.setFont(20)
>>>>>>> 7412770d71b23f0dc3a20708badf476c04975e30


# Define the main menu
def mainMenu():
    global roundNo, winPct, seasonRecord
    roundNo = 1
    winPct = list()
    seasonRecord = [0]
    app.removeAllWidgets()
    app.addLabel("welcome", "Choose an Option to Begin Playing:",1, 1)
    app.addEmptyLabel(2, 1, 5)
    app.addEmptyLabel(3, 1, 5)
    app.addEmptyLabel(4, 1, 5)
    app.addButtons(["With Playoffs", "Without Playoffs", "Quit"], press, 11, 1, 5)

# Define functions resulting from buttons on main menu
def press(name):
    global playoffs
    if name=="With Playoffs":
        playoffs = 1
        app.removeAllWidgets()
        draftmenu(name)

    elif name=="Without Playoffs":
        playoffs = 0
        app.removeAllWidgets()
        draftmenu(name)
    else:
        app.stop()

# Generate number of teams in the league for use in draft game, advance to next menu
def draftmenu(name):
    app.addLabel("teamsel", "Choose Game Settings",0,1,5)
    app.addLabelOptionBox("Number of Teams:", ["- Options -", "2", "3", "4", "5", "6", "7", "8", "9", "10"], 1, 2, 3)
    app.addLabelOptionBox("Salary Cap:", ["- Options -", "$20,000,000", "$25,000,000", "$30,000,000", "$35,000,000", "$40,000,000"], 2, 2, 3)
    app.addButtons(["Home", "Submit"], [home, goDraft], 11,1,5)

# Function to return to main menu
def home(name):
    app.removeAllWidgets()
    mainMenu()

# Generate team stats dictionary (initialized to 0)
def goDraft(name):
    global teams, salaryCap
    teams = int(app.getOptionBox("Number of Teams:"))
    salaryCap = int(''.join(app.getOptionBox("Salary Cap:").split('$')[1].split(',')))
    for i in range(teams):
        teamStats[i] = {'OBP':0, 'SLG':0, 'Index':0, 'Payroll':0}
    rosterSelect(None)


def rosterSelect(name):
    # Refresh page with options to choose the roster file .csv that will be used
    app.removeAllWidgets()
    app.addLabel("fileGet", "Choose the Roster File:",0,1,5)
    app.addLabel("Select a File:", 1, 2, 2)
    app.addButton("Click Here to Select File", openFile, 1, 2, 4)
    app.addEmptyLabel("file", 2,1,5)
    app.addButtons(["Home", "Submit"], [home, buildRoster], 11,1,5)

# Grab the path to the roster file, save in global variable
def openFile(name):
    global teamFilePath
    teamFilePath = app.openBox("Roster", fileTypes=[('CSV', '*.csv')])
    app.setLabel("file", teamFilePath)

def buildRoster(name):
    global draftBoard, teamFilePath
    try:
        roster = open(teamFilePath, 'r')
        reader = csv.reader(roster, delimiter=',')
        draftBoard = list(reader)
        startDraft(None)
    except:
        app.warningBox("Invalid File", "You did not select a valid roster file. Please select a roster file before continuing.")
        rosterSelect(None)

# Present the selection options to be used in the drafting process, and the draft order.
# Also creates the draft board as a list of lists from the indicated csv
def startDraft(name):
    global draftBoard, teamStats, cTeam, teams, teamOrder, salaryCap
    app.removeAllWidgets()
    app.addLabel("draft", "Player Draft", 0,0,10)
    teamOrder = list(range(1,teams+1))
    random.shuffle(teamOrder)
    app.addLabel("roundTeam", "Round 1, Team " + str(teamOrder[0]),1,1,3)
    app.addLabel("roundOrder", "Round 1: " + str(teamOrder),2,1,3)
    app.addMeter("capFill", 4,1,3,1)
    app.setMeterFill("capFill", "green")
    app.setMeter("capFill", 0, "$" + str(0))
    app.addLabelOptionBox("Select a Player:",  [item[0] + ": " + item[1]+ " " + item[2] for item in draftBoard[1:] if item[7]=='0'], 5,1,3)
    app.addButtons(["Home", "Submit"], [home, makePick], 11,1,5)

def makePick(name):
    global cTeam, teamStats, roundNo, teams, salaryCap

    # Mark player drafted
    pNo = int(app.getOptionBox("Select a Player:").split(':')[0])
    draftBoard[pNo+1][7] = 1


    # Add player stats to selecting team
    if (roundNo%2)==1:
        teamStats[teamOrder[cTeam]-1]['OBP']+=(float(draftBoard[pNo+1][4])/9)
        teamStats[teamOrder[cTeam]-1]['SLG']+=(float(draftBoard[pNo+1][5])/9)
        teamStats[teamOrder[cTeam]-1]['Index']+=(float(draftBoard[pNo+1][6])/9)
        teamStats[teamOrder[cTeam]-1]['Payroll']+=(float(draftBoard[pNo+1][8]))
    else:
        teamStats[teamOrder[teams-(1+cTeam)]-1]['OBP']+=(float(draftBoard[pNo+1][4])/9)
        teamStats[teamOrder[teams-(1+cTeam)]-1]['SLG']+=(float(draftBoard[pNo+1][5])/9)
        teamStats[teamOrder[teams-(1+cTeam)]-1]['Index']+=(float(draftBoard[pNo+1][6])/9)
        teamStats[teamOrder[teams-(1+cTeam)]-1]['Payroll']+=(float(draftBoard[pNo+1][8]))

    advance()

    capCheck()

    # End the draft
    if roundNo>9:
        stats()
    else:
        if (roundNo%2)==1:
            app.setLabel("roundTeam", "Round " + str(roundNo) + ", Team " + str(teamOrder[cTeam]))
            app.setMeter("capFill", int(100*teamStats[teamOrder[cTeam]-1]['Payroll']/salaryCap), "Payroll: $" + str(teamStats[teamOrder[cTeam]-1]['Payroll']))
            app.changeOptionBox("Select a Player:", [item[0] + ": " + item[1]+ " " + item[2] for item in draftBoard[1:] if item[7]=='0'])
        else:
            app.setLabel("roundTeam", "Round " + str(roundNo) + ", Team " + str(list(reversed(teamOrder))[cTeam]))
            app.setMeter("capFill", int(100*teamStats[teamOrder[teams-(1+cTeam)]-1]['Payroll']/salaryCap), "Payroll: $" + str(teamStats[teamOrder[teams-(1+cTeam)]-1]['Payroll']))
            app.changeOptionBox("Select a Player:", [item[0] + ": " + item[1]+ " " + item[2] for item in draftBoard[1:] if item[7]=='0'])



def advance():
    global cTeam, roundNo, teams
    cTeam+=1

    if cTeam==(teams):
        cTeam=0
        roundNo+=1

def capCheck():
    global cTeam, teamStats, roundNo, teams
    if (roundNo%2)==1:
        if teamStats[teamOrder[cTeam]-1]['Payroll']>salaryCap:
            teamStats[teamOrder[cTeam]-1]['OBP']+=(float(draftBoard[0][4])/9)
            teamStats[teamOrder[cTeam]-1]['SLG']+=(float(draftBoard[0][5])/9)
            teamStats[teamOrder[cTeam]-1]['Index']+=(float(draftBoard[0][6])/9)
            teamStats[teamOrder[cTeam]-1]['Payroll']+=(float(draftBoard[0][8]))
            app.warningBox("Salary Cap Exceeded", "Team " + str(teamOrder[cTeam]) + " has exceeded the salary cap and has signed a 'Mendoza Player' to the roster.")
            advance()
            # End the draft
            if roundNo>9:
                stats()
            else:
                capCheck()
    else:
        if teamStats[teamOrder[teams-(1+cTeam)]-1]['Payroll']>salaryCap:
            teamStats[teamOrder[teams-(1+cTeam)]-1]['OBP']+=(float(draftBoard[0][4])/9)
            teamStats[teamOrder[teams-(1+cTeam)]-1]['SLG']+=(float(draftBoard[0][5])/9)
            teamStats[teamOrder[teams-(1+cTeam)]-1]['Index']+=(float(draftBoard[0][6])/9)
            teamStats[teamOrder[teams-(1+cTeam)]-1]['Payroll']+=(float(draftBoard[0][8]))
            app.warningBox("Salary Cap Exceeded", "Team " + str(list(reversed(teamOrder))[cTeam]) + " has exceeded the salary cap and has signed a 'Mendoza Player' to the roster.")
            advance()
            # End the draft
            if roundNo>9:
                stats()
            else:
                capCheck()

def stats():
    global teamStats, teams
    app.removeAllWidgets()
    best = 0
    app.addLabel("teamWin", "Team Offensive Indexes", 0,1,1)
    for i in range(teams):
        if teamStats[i]['Index']>best:
            best = teamStats[i]['Index']
    for i in range(teams):
        app.addLabel("teamL"+str(i+1), "Team " + str(i+1), i+1,1,1)
        app.addMeter("team"+str(i+1), i+1, 2, 3, 1)
        app.setMeterFill("team"+str(i+1), "red")
        app.setMeter("team"+str(i+1), int(100*teamStats[i]['Index']/best), "Index: " + str(round(teamStats[i]['Index'], 3)))
    app.addButtons(["Home", "Proceed"], [home, season], 11,1,5)

def season(name):
    global playoffs, teams, teamStats, seasonRecord, winPct
    app.removeAllWidgets()

    for x in range(1, teams):
        seasonRecord.append(0)
    for x in range(teams):
        for y in range(x+1, teams):
            record = series(teamStats[x], teamStats[y], 10)
            seasonRecord[x] += record
            seasonRecord[y] += (10-record)

    for i in range(teams):
        winPct.append(seasonRecord[i]/(10*(teams-1)))

    app.addLabel("teamWin", "Team Regular Season Win Percentages", 0,1,5)
    for i in range(teams):
        app.addLabel("teamL"+str(i+1), "Team " + str(i+1), i+1,1,1)
        app.addMeter("team"+str(i+1), i+1, 2, 3, 1)
        app.setMeterFill("team"+str(i+1), "lightBlue")
        app.setMeter("team"+str(i+1), int(100*winPct[i]), "Win %: " + str(round(winPct[i], 3)))

    if playoffs == 1:
        app.addButtons(["Home", "Proceed"], [home, playoff], 11,1,5)
    else:
        app.addButton("Home", home, teams+1,1,5)

def playoff(name):
    global winPct, teams
    winPct = sorted(list(zip(winPct, list(range(teams)))))
    if teams==2: bracket2(None)
    elif teams==3: bracket3(None)
    elif teams==4: bracket4(None)
    elif teams==5: bracket5(None)
    elif teams==6: bracket6(None)
    elif teams==7: bracket7(None)
    elif teams==8: bracket8(None)
    elif teams==9: bracket9(None)
    elif teams==10: bracket10(None)
    else: mainMenu()

def bracket2(name):
    global teamStats, winPct
    app.removeAllWidgets()
    app.addLabel("results", "Playoff Results", 1,1,5)
    app.addLabel("champSer", "Championship Series: Team " + str(winPct[0][1]+1) + " vs Team " + str(winPct[1][1]+1), 2,1,5)
    game1 = series(teamStats[winPct[0][1]], teamStats[winPct[1][1]], 7)
    if (game1 > 3):
        app.addLabel("winner", "Team " + str(winPct[0][1] + 1) + " Wins!", 3,1,5)
    else:
        app.addLabel("winner", "Team " + str(winPct[1][1] + 1) + " Wins!", 3,1,5)

    app.addButton("Home", home, 11,1,5)

def bracket3(name):
    global teamStats, winPct
    app.removeAllWidgets()
    app.addLabel("results", "Playoff Results", 1, 1, 5)
    app.addLabel("roundLabel", "Semi-Final: Team " + str(winPct[0][1] + 1) + " vs Team " + str(winPct[1][1] + 1), 2, 1, 5)
    game1 = series(teamStats[winPct[0][1]], teamStats[winPct[1][1]], 7)
    if (game1 > 3):
        app.addLabel("winner", "Team " + str(winPct[0][1] + 1) + " Advances!", 3,1,5)
        winPct.pop(1)
    else:
        app.addLabel("winner", "Team " + str(winPct[1][1] + 1) + " Advances!", 3,1,5)
        winPct.pop(0)
    app.addButtons(["Home", "Continue"], [home, bracket2], 11,1,5)

def bracket4(name):
    global teamStats, winPct
    app.removeAllWidgets()
    drop = list()
    app.addLabel("results", "Playoff Results", 1, 1, 5)
    app.addLabel("roundLabel1", "Semi-Final 1: Team " + str(winPct[0][1] + 1) + " vs Team " + str(winPct[3][1] + 1), 2, 1, 5)
    game1 = series(teamStats[winPct[0][1]], teamStats[winPct[3][1]], 7)
    game2 = series(teamStats[winPct[1][1]], teamStats[winPct[2][1]], 7)
    if (game1 > 3):
        app.addLabel("winner1", "Team " + str(winPct[0][1] + 1) + " Advances!", 3,1,5)
        drop.append(3)
    else:
        app.addLabel("winner1", "Team " + str(winPct[3][1] + 1) + " Advances!", 3,1,5)
        drop.append(0)
    app.addLabel("roundLabel2", "Semi-Final 2: Team " + str(winPct[1][1] + 1) + " vs Team " + str(winPct[2][1] + 1), 4, 1, 5)
    if (game2 > 3):
        app.addLabel("winner2", "Team " + str(winPct[1][1] + 1) + " Advances!", 5,1,5)
        drop.append(2)
    else:
        app.addLabel("winner2", "Team " + str(winPct[2][1] + 1) + " Advances!", 5,1,5)
        drop.append(1)
    drop = sorted(drop)
    winPct.pop(drop.pop())
    winPct.pop(drop.pop())
    app.addButtons(["Home", "Continue"], [home, bracket2], 11,1,5)

def bracket5(name):
    global teamStats, winPct
    app.removeAllWidgets()
    app.addLabel("results", "Playoff Results", 1, 1, 5)
    app.addLabel("roundLabel1", "Play-in Game: Team " + str(winPct[0][1] + 1) + " vs Team " + str(winPct[1][1] + 1), 2, 1, 5)
    game1 = series(teamStats[winPct[0][1]], teamStats[winPct[1][1]], 7)
    if (game1 > 3):
        app.addLabel("winner", "Team " + str(winPct[0][1] + 1) + " Advances!", 3,1,5)
        winPct.pop(1)
    else:
        app.addLabel("winner", "Team " + str(winPct[1][1] + 1) + " Advances!", 3,1,5)
        winPct.pop(0)
    app.addButtons(["Home", "Continue"], [home, bracket4], 11,1,5)

def bracket6(name):
    global teamStats, winPct
    app.removeAllWidgets()
    drop = list()
    app.addLabel("results", "Playoff Results", 1, 1, 5)
    app.addLabel("roundLabel1", "Quarter-Final 1: Team " + str(winPct[0][1] + 1) + " vs Team " + str(winPct[3][1] + 1), 2, 1, 5)
    game1 = series(teamStats[winPct[0][1]], teamStats[winPct[3][1]], 7)
    game2 = series(teamStats[winPct[1][1]], teamStats[winPct[2][1]], 7)
    if (game1 > 3):
        app.addLabel("winner1", "Team " + str(winPct[0][1] + 1) + " Advances!", 3,1,5)
        drop.append(3)
    else:
        app.addLabel("winner1", "Team " + str(winPct[3][1] + 1) + " Advances!", 3,1,5)
        drop.append(0)
    app.addLabel("roundLabel2", "Quarter-Final 2: Team " + str(winPct[1][1] + 1) + " vs Team " + str(winPct[2][1] + 1), 4, 1, 5)
    if (game2 > 3):
        app.addLabel("winner2", "Team " + str(winPct[1][1] + 1) + " Advances!", 5,1,5)
        drop.append(2)
    else:
        app.addLabel("winner2", "Team " + str(winPct[2][1] + 1) + " Advances!", 5,1,5)
        drop.append(1)
    drop = sorted(drop)
    winPct.pop(drop.pop())
    winPct.pop(drop.pop())
    app.addButtons(["Home", "Continue"], [home, bracket4], 11,1,5)

def bracket7(name):
    global teamStats, winPct
    app.removeAllWidgets()
    drop = list()
    app.addLabel("results", "Playoff Results", 1, 1, 5)
    app.addLabel("roundLabel1", "Quarter-Final 1: Team " + str(winPct[0][1] + 1) + " vs Team " + str(winPct[5][1] + 1), 2, 1, 5)
    game1 = series(teamStats[winPct[0][1]], teamStats[winPct[5][1]], 7)
    game2 = series(teamStats[winPct[1][1]], teamStats[winPct[4][1]], 7)
    game3 = series(teamStats[winPct[2][1]], teamStats[winPct[3][1]], 7)
    if (game1 > 3):
        app.addLabel("winner1", "Team " + str(winPct[0][1] + 1) + " Advances!", 3,1,5)
        drop.append(5)
    else:
        app.addLabel("winner1", "Team " + str(winPct[5][1] + 1) + " Advances!", 3,1,5)
        drop.append(0)
    app.addLabel("roundLabel2", "Quarter-Final 2: Team " + str(winPct[1][1] + 1) + " vs Team " + str(winPct[4][1] + 1), 4, 1, 5)
    if (game2 > 3):
        app.addLabel("winner2", "Team " + str(winPct[1][1] + 1) + " Advances!", 5,1,5)
        drop.append(4)
    else:
        app.addLabel("winner2", "Team " + str(winPct[4][1] + 1) + " Advances!", 5,1,5)
        drop.append(1)
    app.addLabel("roundLabel3", "Quarter-Final 3: Team " + str(winPct[2][1] + 1) + " vs Team " + str(winPct[3][1] + 1), 6, 1, 5)
    if (game3 > 3):
        app.addLabel("winner3", "Team " + str(winPct[2][1] + 1) + " Advances!", 7,1,5)
        drop.append(3)
    else:
        app.addLabel("winner3", "Team " + str(winPct[3][1] + 1) + " Advances!", 7,1,5)
        drop.append(2)
    drop = sorted(drop)
    for i in range(3):
        winPct.pop(drop.pop())
    app.addButtons(["Home", "Continue"], [home, bracket4], 11,1,5)

def bracket8(name):
    global teamStats, winPct
    app.removeAllWidgets()
    drop = list()
    app.addLabel("results", "Playoff Results", 1, 1, 5)
    game1 = series(teamStats[winPct[0][1]], teamStats[winPct[7][1]], 7)
    game2 = series(teamStats[winPct[1][1]], teamStats[winPct[6][1]], 7)
    game3 = series(teamStats[winPct[2][1]], teamStats[winPct[5][1]], 7)
    game4 = series(teamStats[winPct[3][1]], teamStats[winPct[4][1]], 7)
    app.addLabel("roundLabel1", "Quarter-Final 1: Team " + str(winPct[0][1] + 1) + " vs Team " + str(winPct[7][1] + 1), 2, 1, 5)
    if (game1 > 3):
        app.addLabel("winner1", "Team " + str(winPct[0][1] + 1) + " Advances!", 3,1,5)
        drop.append(7)
    else:
        app.addLabel("winner1", "Team " + str(winPct[7][1] + 1) + " Advances!", 3,1,5)
        drop.append(0)
    app.addLabel("roundLabel2", "Quarter-Final 2: Team " + str(winPct[1][1] + 1) + " vs Team " + str(winPct[6][1] + 1), 4, 1, 5)
    if (game2 > 3):
        app.addLabel("winner2", "Team " + str(winPct[1][1] + 1) + " Advances!", 5,1,5)
        drop.append(6)
    else:
        app.addLabel("winner2", "Team " + str(winPct[6][1] + 1) + " Advances!", 5,1,5)
        drop.append(1)
    app.addLabel("roundLabel3", "Quarter-Final 3: Team " + str(winPct[2][1] + 1) + " vs Team " + str(winPct[5][1] + 1), 6, 1, 5)
    if (game3 > 3):
        app.addLabel("winner3", "Team " + str(winPct[2][1] + 1) + " Advances!", 7,1,5)
        drop.append(5)
    else:
        app.addLabel("winner3", "Team " + str(winPct[5][1] + 1) + " Advances!", 7,1,5)
        drop.append(2)
    app.addLabel("roundLabel4", "Quarter-Final 4: Team " + str(winPct[3][1] + 1) + " vs Team " + str(winPct[4][1] + 1), 8, 1, 5)
    if (game4 > 3):
        app.addLabel("winner4", "Team " + str(winPct[3][1] + 1) + " Advances!", 9,1,5)
        drop.append(4)
    else:
        app.addLabel("winner4", "Team " + str(winPct[4][1] + 1) + " Advances!", 9,1,5)
        drop.append(3)
    drop = sorted(drop)
    for i in range(4):
        winPct.pop(drop.pop())
    app.addButtons(["Home", "Continue"], [home, bracket4], 11,1,5)

def bracket9(name):
    global teamStats, winPct
    app.removeAllWidgets()
    app.addLabel("results", "Playoff Results", 1, 1, 5)
    app.addLabel("roundLabel1", "Wild-Card Game: Team " + str(winPct[0][1] + 1) + " vs Team " + str(winPct[1][1] + 1), 2, 1, 5)
    game1 = series(teamStats[winPct[0][1]], teamStats[winPct[1][1]], 7)
    if (game1 > 3):
        app.addLabel("winner", "Team " + str(winPct[0][1] + 1) + " Advances!", 3,1,5)
        winPct.pop(1)
    else:
        app.addLabel("winner", "Team " + str(winPct[1][1] + 1) + " Advances!", 3,1,5)
        winPct.pop(0)
    app.addButtons(["Home", "Continue"], [home, bracket8], 11,1,5)

def bracket10(name):
    global teamStats, winPct
    app.removeAllWidgets()
    drop = list()
    app.addLabel("results", "Playoff Results", 1, 1, 5)
    app.addLabel("roundLabel1", "Wild-Card Game 1: Team " + str(winPct[0][1] + 1) + " vs Team " + str(winPct[3][1] + 1), 2, 1, 5)
    game1 = series(teamStats[winPct[0][1]], teamStats[winPct[3][1]], 7)
    game2 = series(teamStats[winPct[1][1]], teamStats[winPct[2][1]], 7)
    if (game1 > 3):
        app.addLabel("winner1", "Team " + str(winPct[0][1] + 1) + " Advances!", 3,1,5)
        drop.append(3)
    else:
        app.addLabel("winner1", "Team " + str(winPct[3][1] + 1) + " Advances!", 3,1,5)
        drop.append(0)
    app.addLabel("roundLabel2", "Wild-Card Game 2: Team " + str(winPct[1][1] + 1) + " vs Team " + str(winPct[2][1] + 1), 4, 1, 5)
    if (game2 > 3):
        app.addLabel("winner2", "Team " + str(winPct[1][1] + 1) + " Advances!", 5,1,5)
        drop.append(2)
    else:
        app.addLabel("winner2", "Team " + str(winPct[2][1] + 1) + " Advances!", 5,1,5)
        drop.append(1)
    drop = sorted(drop)
    winPct.pop(drop.pop())
    winPct.pop(drop.pop())
    app.addButtons(["Home", "Continue"], [home, bracket8], 11,1,5)

def game(team1, team2):
    win1 = .5 - 2.032 * (team1['OBP'] - team2['OBP']) - 0.9 * (team1['SLG'] - team2['SLG'])
    draw = random.uniform(0,1)

    return int(draw>=win1)

def series(team1, team2, games=1):
    wins = [game(team1, team2)]
    for i in range(1,games):
        wins.append(game(team1, team2))
    return int(sum(wins))


teamOrder = None
roundNo = 1
teams = 0
teamFilePath = None
playoffs = 0
draftBoard = None
salaryCap = 20000000
cTeam = 0
teamStats = dict()
seasonRecord = [0]
winPct = list()

mainMenu()
app.go()
