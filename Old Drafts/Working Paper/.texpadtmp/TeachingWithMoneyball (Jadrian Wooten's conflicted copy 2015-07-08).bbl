\begin{thebibliography}{}

\bibitem[\protect\citeauthoryear{Becker and Watts}{Becker and
  Watts}{1995}]{BeckerWatts1995}
Becker, W. and M.~Watts (1995).
\newblock Teaching tools: Teaching methods in undergraduate economics.
\newblock {\em Economic Inquiry\/}~{\em 33\/}(4), 692--700.

\bibitem[\protect\citeauthoryear{Day}{Day}{2005}]{NetDay2005}
Day, N. (2005).
\newblock Speak up 2005.

\bibitem[\protect\citeauthoryear{Hakes and Sauer}{Hakes and
  Sauer}{2006}]{HakesSaur}
Hakes, J.~K. and R.~D. Sauer (2006).
\newblock An economic evaluation of the \emph{Moneyball} hypothesis.
\newblock {\em Journal of Economic Perspectives\/}~{\em 20\/}(3), 173--185.

\bibitem[\protect\citeauthoryear{Lean, Moizer, Towler, and Abbey}{Lean
  et~al.}{2006}]{LeanEtAl}
Lean, J., J.~Moizer, M.~Towler, and C.~Abbey (2006).
\newblock Simulations and games: Use and barriers in higher education.
\newblock {\em Active Learning in Higher Education\/}~{\em 7\/}(3), 227--242.

\bibitem[\protect\citeauthoryear{{National Center for Education Statistics
  (NCES)}}{{National Center for Education Statistics (NCES)}}{2000}]{NCES200}
{National Center for Education Statistics (NCES)} (2000).
\newblock Teachers’ tools for the 21st century: A report on teachers’ use
  of technology.

\bibitem[\protect\citeauthoryear{Pasin and Giroux}{Pasin and
  Giroux}{2011}]{Pasin2011}
Pasin, F. and H.~Giroux (2011).
\newblock The impact of a simulation game on operations management education.
\newblock {\em Computers and Education\/}~{\em 57\/}(1), 1240--1254.

\bibitem[\protect\citeauthoryear{Rakes and Casey}{Rakes and
  Casey}{2002}]{RakesCasey2002}
Rakes, C. and H.~Casey (2002).
\newblock An analysis of teacher concerns toward instructional technology.
\newblock {\em International Journal of Educational Technology [Electronic
  version]\/}~{\em 3\/}(1).

\bibitem[\protect\citeauthoryear{Rocca}{Rocca}{2010}]{Rocca2010}
Rocca, K. (2010).
\newblock Student participation in the college classroom: An extended
  multidisciplinary literature review.
\newblock {\em Communication Education\/}~{\em 59}, 185--213.

\bibitem[\protect\citeauthoryear{Shellman and Turan}{Shellman and
  Turan}{2006}]{Shellman2006}
Shellman, S.~M. and K.~Turan (2006).
\newblock Do simulations enhance student learning? an empirical evaluation of
  an ir simulation.
\newblock {\em Journal of Political Science Education\/}~{\em 2\/}(1), 19--32.

\bibitem[\protect\citeauthoryear{Siddiqui, Khan, and Akhtar}{Siddiqui
  et~al.}{2008}]{Siddiqui2008}
Siddiqui, A., M.~Khan, and S.~Akhtar (2008).
\newblock Supply chain simulator: A scenario-based educational tool to enhance
  student learning.
\newblock {\em Computers and Education\/}~{\em 51\/}(1), 252 -- 261.

\bibitem[\protect\citeauthoryear{Van~Wyk}{Van~Wyk}{2011}]{kamla2011}
Van~Wyk, M.~M. (2011).
\newblock The effects of teams-games-tournaments on achievement, retention, and
  attitudes of economics education students.
\newblock {\em Journal Social Science\/}~{\em 26\/}(3), 183--193.

\bibitem[\protect\citeauthoryear{Watts and Schaur}{Watts and
  Schaur}{2011}]{Watts2011}
Watts, M. and G.~Schaur (2011).
\newblock Teaching and assessment methods in undergraduate economics: A fourth
  national quinquennial survey.
\newblock {\em The Journal of Economic Education\/}~{\em 42\/}(3), 294--309.

\end{thebibliography}
